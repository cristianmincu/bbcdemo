//
//  News.swift
//  BBCnews
//
//  Created by Mincu, Cristian on 2019-03-24.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import Foundation

public struct News {
    var title: String?
    var publicationDate: String?
    var imageURL: String?
    var newsURL: String?
    var newsDescription: String?
    
    
    func formattedDate() -> String? {
        guard let date = publicationDate else {
            return ""
        }
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "E, d MMM yyyy HH:mm:ss Z"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/yyyy"
        
        if let newsDate = dateFormatterGet.date(from: date) {
            return dateFormatter.string(for: newsDate)
        }
        
        return ""
    }
}
