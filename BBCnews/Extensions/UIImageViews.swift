//
//  UIImageViews.swift
//  BBCnews
//
//  Created by Mincu, Cristian on 2019-03-25.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import UIKit
import AlamofireImage

extension UIImageView{
    
    public func getRemoteImage(url: String, placeholder: String){
        
        self.af_setImage(withURL: URL(string: url)!, placeholderImage: UIImage(named: ""), filter: nil, progress: nil, progressQueue: DispatchQueue.main, imageTransition: .crossDissolve(0.5), runImageTransitionIfCached: true, completion: { response in
            if response.response != nil {
                print("Image loaded")
            }
            else
            {
                print("failed to load Image from \(url)")
            }
        })
        
    }
}
