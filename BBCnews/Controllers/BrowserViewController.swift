//
//  BrowserViewController.swift
//  BBCnews
//
//  Created by Mincu, Cristian on 2019-03-24.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//

import UIKit
import WebKit

class BrowserViewController: UIViewController {

    var webView: WKWebView!
    var webURL: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setup()
    }
    
    private func setup() {
        guard let webURL = webURL, let url = URL(string: webURL) else {
            return
        }

        webView = WKWebView()
        webView.load(URLRequest(url: url))
        view = webView
    }

}
