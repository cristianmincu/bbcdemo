//
//  NewsCell.swift
//  BBCnews
//
//  Created by Mincu, Cristian on 2019-03-24.
//  Copyright © 2019 Mincu, Cristian. All rights reserved.
//


import UIKit

class NewsCell: UITableViewCell {
    
    @IBOutlet weak var newsImage: UIImageView!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    @IBOutlet weak var dateLabel: UILabel!
    
    func setup(news: News) {
        titleLabel.text = news.title
        descriptionLabel.text = news.newsDescription
        dateLabel.text = news.formattedDate()

        setupImage(url: news.imageURL)
    }
    
    private func setupImage(url: String?) {
        guard let url = url else {
            return
        }
        
        newsImage.getRemoteImage(url: url, placeholder: "")
    }
    
}
